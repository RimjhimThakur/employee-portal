from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
from myApp.models import myIp,employee,emp_details,leave,document,Notification,HR,leaveAssign,leaveApply,announcement,holiday,attendance,event,HrNotification,attendance_user,comments,contactus
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import datetime
import random
from myApp.forms import PersonalInfo,HealthInfo,FamilyInfo,inputsalary,inputjob
from django.core.mail import EmailMessage
# from requests import get
# Create your views here.
import socket
ip =socket.gethostbyname(socket.gethostname())
myips = myIp.objects.all()
ip_list = [i.value for i in myips]

def home(request):
    
    all_feed = comments.objects.all().order_by('-id')[:5]
    if 'id' in request.COOKIES:
        id = request.COOKIES['id']
        user = User.objects.get(id=id)
        login(request,user)
        if user.is_staff:
            return HttpResponseRedirect('/myApp/hrdash')
        if user.is_active:
            return HttpResponseRedirect('/myApp/index')
    if request.method=="POST":
        if "blogcomm" in request.POST:
            txt = request.POST['text']
            name = request.POST['name']
            getText = comments(text=txt,name=name)
            getText.save()
            
    if request.method=="POST":
        if "contact" in request.POST:
            name = request.POST['name']
            email = request.POST['email']
            subject = request.POST['subject']
            message = request.POST['message']
            getData = contactus(name=name,email=email,subject=subject,message=message)
            getData.save()
            return render(request,'HomePage.html',{'status':'Request sent successfully!!!!'})
    return render(request,'HomePage.html',{'all':all_feed})

### Match OTP
def matchotp(request):
    if "forgot" in request.GET:     
        otp = request.GET['motp']
        mtp = request.GET['mtp']
        if otp==mtp:
            return HttpResponse('true')
        else:
            return HttpResponse('false')

def uslogin(request):
    if request.method == 'POST': 
        if "forgot" in request.POST:
            usname = request.POST['usname']
            passw = request.POST['npass']
            usr = User.objects.get(username=usname)
            usr.set_password(passw)
            usr.save()
            return render(request,'newlogin.html',{'status1':'Password changed successfully!!'})
        
        if 'SignUp' in request.POST:
            
            fsname = request.POST['fname']
            lsname = request.POST['lname']
            n = request.POST['uname']
            p = request.POST['pass']
            cn = request.POST['country']
            s = request.POST['state']
            ct = request.POST['city']
            ph = request.POST['phone']    
            res = 'Hello {}!! you have signed up successfully'.format(n)
            userData = User.objects.create_user(n,'',p)
            userData.first_name = fsname
            userData.last_name = lsname
            userData.is_active=False
            userData.save()

            emp = employee(user=userData,country=cn,state=s,city=ct,contact=ph)
            emp.save()
            if "image" in request.FILES:
                emp.image = request.FILES["image"]
            emp.save()
            
            detail = emp_details(emp_name=emp)
            detail.save()

            msz = 'New user for signup!!!'
            notify = HrNotification(content=msz,notify_type="signup")
            notify.save()

            response = HttpResponseRedirect('/myApp/personal_info',{'status':msz})
            response.set_cookie('employee',detail.id)
            return response 

        if 'SignIn' in request.POST:
            username = request.POST['username']
            password = request.POST['password']

            user = authenticate(username=username,password=password)
            if user:
                login(request,user)
                if user.is_staff:
                    response =  HttpResponseRedirect('/myApp/hrdash')
                    if "remember" in request.POST:                      
                        response.set_cookie('username',username)
                        response.set_cookie('id',user.id)
                        response.set_cookie('logintime',datetime.datetime.now())
                    return response
                   
                elif user.is_active:
                    response =  HttpResponseRedirect('/myApp/index')
                    if "remember" in request.POST:
                        response.set_cookie('username',username)
                        response.set_cookie('id',user.id)
                        response.set_cookie('logintime',datetime.datetime.now())
                    return response
                    
                else:
                    return render(request,'newlogin.html',{'status':'You are not allowed!!'})    
            else:
                return render(request,'newlogin.html',{'status1':'Invalid Login Details'})

    return render(request,'newlogin.html')

def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('id')
    return response

def personal_info(request):
    if 'employee' in request.COOKIES:
        id = request.COOKIES['employee']

    instance = get_object_or_404(emp_details, id=id)
    form = PersonalInfo(request.POST or None, instance=instance)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/myApp/health_record')

    return render(request,'pinfoform.html',{'form':form})

def health_info(request):
    if 'employee' in request.COOKIES:
        id = request.COOKIES['employee']

    instance = get_object_or_404(emp_details, id=id)
    form = HealthInfo(request.POST or None, instance=instance)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/myApp/family_record')

    return render(request,'health_record.html',{'form':form})

def input_salary(request):
    id=request.GET['id']
    emp_det = employee.objects.get(id=id)
    details = emp_details.objects.get(emp_name__id=emp_det.id)

    frm = inputsalary()
    if request.method=="POST":
        frm = inputsalary(instance=details, data=request.POST)
        if frm.is_valid():
            instance = frm.save(commit=False)     
            instance.save()
            return HttpResponseRedirect('/myApp/hrdash',{'status':'Updated successfully!!!!'})
    return render(request,'input_salary.html',{'form':frm,'details':emp_det,})

def input_job(request):
    id=request.GET['id']
    emp_det = employee.objects.get(id=id)
    details = emp_details.objects.get(emp_name__id=emp_det.id)
    user = User.objects.get(username=request.user.username)
    # emp = employee.objects.get(user=user)
    # emp_det = emp_details.objects.get(emp_name=emp)
    frm = inputjob()
    if request.method=="POST":
        frm = inputjob(instance=details, data=request.POST)
        if frm.is_valid():
            instance = frm.save(commit=False)     
            instance.save()
            return HttpResponseRedirect('/myApp/input_salary?id='+str(emp_det.id))
    return render(request,'input_job.html',{'form':frm,'details':emp_det})


def family_info(request):
    if 'employee' in request.COOKIES:
        id = request.COOKIES['employee']

    instance = get_object_or_404(emp_details, id=id)
    form = FamilyInfo(request.POST or None, instance=instance)
    if form.is_valid():
        form.save()
        
        response = HttpResponseRedirect('/myApp/login')
        response.delete_cookie('employee')
        return response

    return render(request,'family_record.html',{'form':form})


def check_user(request):
    un = request.GET['uname']
    check = User.objects.filter(username=un)
    if len(check)!=0:
        return HttpResponse("A user with this name already exists!!")
    else:
        return HttpResponse("Username Validation Success!!!")

def index(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)

    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    abcd = datetime.datetime.now().date()
    get_attendance_in = attendance_user.objects.filter(date=abcd, emp_name__user__username=request.user.username,attendance_type='checkin')
    get_attendance_out = attendance_user.objects.filter(date=abcd, emp_name__user__username=request.user.username,attendance_type='checkout')
    d = announcement.objects.all().order_by('-id')[:6]
    show = 1
    inlength = len(get_attendance_in)
    outlength = len(get_attendance_out)
    if inlength > 0 and outlength > 0:
        show = 0
    for i in d:
        print(i.text)
    e = event.objects.all().order_by('-id')[:6]
    for i in e:
        print(i.event_text)
    ctime=''
    cout=''
    if "checkintime" in request.COOKIES:
        ctime = str(request.COOKIES['checkintime']).split('.')[0]
    if "checkouttime" in request.COOKIES:
        cout = str(request.COOKIES['checkouttime']).split('.')[0]
    return render(request,'dashboard.html',{'details':emp_det,'employee':emp,'notify':an,'totalnot':total,'d':d,'e':e, 'user_in':inlength, 'user_out':outlength, 'show':show,'ctime':ctime,'cout':cout})

def leaveView(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)


    leave_det = leaveApply.objects.filter(emp_id=emp).order_by('-id')
    leave_app = leaveApply.objects.filter(emp_id=emp)
    
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)

    cltype = leaveAssign.objects.get(ltype="CL")
    leave_taken = leaveApply.objects.filter(emp_id=emp,status="Approve",leave_type=cltype)
    df=0
    for i in leave_taken:
        df+=i.for_days
    balance=cltype.total-df
    applied = len(leave_taken)
    cl_status = {'c_bal':balance,'c_taken':df,'c_app':applied,'date':cltype.date}

    pltype = leaveAssign.objects.get(ltype="PL")
    p_leave_taken = leaveApply.objects.filter(emp_id=emp,status="Approve",leave_type=pltype)
    p_df=0
    for i in p_leave_taken:
        p_df+=i.for_days
    p_balance=pltype.total-p_df
    p_applied = len(p_leave_taken)
    pl_status = {'p_bal':p_balance,'p_taken':p_df,'p_app':p_applied,'date':pltype.date}

    coff_type = leaveAssign.objects.get(ltype="Coff")
    coff_leave_taken = leaveApply.objects.filter(emp_id=emp,status="Approve",leave_type=coff_type)
    coff_df=0
    for i in coff_leave_taken:
        coff_df+=i.for_days
    coff_balance=coff_type.total-coff_df
    coff_applied = len(coff_leave_taken)
    coff_status = {'coff_bal':coff_balance,'coff_taken':coff_df,'coff_app':coff_applied,'date':coff_type.date}

    rh_type = leaveAssign.objects.get(ltype="RH")
    rh_leave_taken = leaveApply.objects.filter(emp_id=emp,status="Approve",leave_type=rh_type)
    rh_df=0
    for i in rh_leave_taken:
        rh_df+=i.for_days
    rh_balance=rh_type.total-rh_df
    rh_applied = len(rh_leave_taken)
    rh_status = {'rh_bal':rh_balance,'rh_taken':rh_df,'rh_app':rh_applied,'date':rh_type.date}

    if request.method == "POST":
        t = request.POST['type']
        f = request.POST['from']
        to = request.POST['to']
        tot = request.POST['days']
        s = request.POST['session']
        txt = request.POST['text']  
        # date = request.POST['date']
        lt = leaveAssign.objects.get(ltype=t)
        # return HttpResponse(balance)
        if lt.ltype=="CL":
            if int(balance)>int(tot):
                lv = leaveApply(emp_id=emp,leave_type=lt,from_date=f,to_date=to,for_days=tot,session=s,description=txt)
                lv.save()
                msz = 'Leave Application sent!!!'
            else:
                msz = 'You have not sufficient leave balance!!!'
        elif lt.ltype=="PL":
            if int(p_balance)>int(tot):
                lv = leaveApply(emp_id=emp,leave_type=lt,from_date=f,to_date=to,for_days=tot,session=s,description=txt)
                lv.save()
                msz = 'Leave Application sent!!!'
            else:
                msz = 'You have not sufficient leave balance!!!'
        elif lt.ltype=="C_off":

            if int(coff_balance)>int(tot):
                lv = leaveApply(emp_id=emp,leave_type=lt,from_date=f,to_date=to,for_days=tot,session=s,description=txt)
                lv.save()
                msz = 'Leave Application sent!!!'
            else:
                msz = 'You have not sufficient leave balance!!!'
        elif lt.ltype=="RH":
            if int(rh_balance)>int(tot):
                lv = leaveApply(emp_id=emp,leave_type=lt,from_date=f,to_date=to,for_days=tot,session=s,description=txt)
                lv.save()
                msz = 'Leave Application sent!!!'
            else:
                msz = 'You have not sufficient leave balance!!!'
        notify = HrNotification(content=msz,notify_type="leave")
        notify.save()
        # return HttpResponseRedirect('/myApp/leave')
        return render(request,'leavesform.html',{'details':emp_det,'employee':emp,'status':'Application sent Successfully!!','status':msz})

    return render(request,'leavesform.html',{'details':emp_det,'employee':emp,'all':leave_det,'cl_status':cl_status,'pl_status':pl_status,'coff_status':coff_status,'rh_status':rh_status,'totalnot':total,'notify':an})

def contact(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    return render(request,'ContactDetails.html',{'details':emp_det,'employee':emp,'notify':an,'totalnot':total})   

def health(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    return render(request,'healthdetail.html',{'details':emp_det,'employee':emp,'notify':an,'totalnot':total})

  

def holidays(request):
    
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)

    d = holiday.objects.all()
    for i in d:
        print(i.date)
        print(i.day)
        print(i.holidays)
        print(i.status)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)

    return render(request,'Holidays.html',{'details':emp_det,'employee':emp,'d':d,'notify':an,'totalnot':total}) 

def job(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    return render(request,'JobDetail.html',{'details':emp_det,'employee':emp,'notify':an,'totalnot':total})  

def salary(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    return render(request,'SalaryDetail.html',{'details':emp_det,'employee':emp,'notify':an,'totalnot':total})       

def submission(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    all_docs = document.objects.filter(doc=emp).order_by('-id')[:6]
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    if request.method=="POST":
        d = request.FILES['document']
        r = request.POST['reference']
        ds=request.POST['description']

        docs=document(doc=emp,document=d,reference=r,description=ds)
        docs.save()
        msz = 'New Document !!!'
        notify = HrNotification(notify_type="document",content=msz)
        notify.save()
        return render(request,'documentationSubmission.html',{'details':emp_det,'employee':emp,'status':'Document uploaded Successfullly!!','all_docs':all_docs,'status':msz})     
    return render(request,'documentationSubmission.html',{'details':emp_det,'employee':emp,'all_docs':all_docs,'notify':an,'totalnot':total})     

def directory(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.filter(emp_name__user__is_active=True)
    all = employee.objects.filter(user__is_active=True)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    if request.method=="POST":
        to_search = request.POST['content']
        all = employee.objects.filter(user__is_active=True,user__first_name__contains=to_search)
        print('all=',len(all))
        return render(request,'Directory.html',{'all':all,'details':emp_det,'employee':emp})
    return render(request,'Directory.html',{'all':all,'details':emp_det,'employee':emp,'notify':an,'totalnot':total})     

def hrdirectory(request):  
    all= emp_details.objects.filter(emp_name__user__is_active=True) 
    if request.method=="POST":
        to_search = request.POST['content']
        all= emp_details.objects.filter(emp_name__user__first_name__contains=to_search,emp_name__user__is_active=True) 
        
        print('all=',len(all))
        return render(request,'hr_directory.html',{'all':all})
    return render(request,'hr_directory.html',{'all':all})     


def family(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    return render(request,'FamilyDetails.html',{'details':emp_det,'employee':emp,'notify':an,'totalnot':total})

def change_profile(request):
    user = User.objects.get(username = request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    if request.method == "POST":
        fs = request.POST['first']
        ls = request.POST['last']
        em = request.POST['email']
        ph = request.POST['phone']
        user.first_name=fs
        user.last_name =ls
        user.email = em
        user.save()
        emp.contact = ph
        emp.save()
        
        return render(request,'change_profile.html',{'details':emp_det,'data':user,'status':'Changes Saved Successfully!!','c':'success','employee':emp})    
    return render(request,'change_profile.html',{'data':user,'details':emp_det,'employee':emp,'notify':an,'totalnot':total})

def HRchange_profile(request):
    user = User.objects.get(username = request.user.username)
    hr = HR.objects.get(user=user)
    nf = checkNotify()
    # emp_det = emp_details.objects.get(emp_name=emp)
    if request.method == "POST":
        fs = request.POST['first']
        ls = request.POST['last']
        em = request.POST['email']
        ph = request.POST['phone']
        user.first_name=fs
        user.last_name =ls
        user.email = em
        user.save()
        hr.contact = ph
        hr.save()
        
        return render(request,'hr_changeProfile.html',{'data':user,'status':'Changes Saved Successfully!!','c':'success','HR':hr})    
    return render(request,'hr_changeProfile.html',{'data':user,'HR':hr,'notifications':nf})


def profile(request):
    user = User.objects.get(username = request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    return render(request,'user_profile.html',{'data':user,'details':emp_det,'employee':emp,'notify':an,'totalnot':total})


def HRprofile(request):
    user = User.objects.get(username = request.user.username)
    hr = HR.objects.get(user=user)
    nf = checkNotify()
    # v = HR.objects.get(user=hr)
    return render(request,'hr_profile.html',{'data':user,'hr':hr,'notifications':nf})


from django.contrib.auth.hashers import check_password
@login_required
def change_password(request):
    user = User.objects.get(username=request.user.username)
    login_user_password = request.user.password
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    if request.method == 'POST':
        current = request.POST['old']
        newpas = request.POST['new']

        check = check_password(current,login_user_password)
        if check==True:
            user.set_password(newpas)
            user.save()
            return render(request,'change_password.html',{'status':'Password changed successfully','col':'success'})
            response = HttpResponseRedirect('/myApp/home')
            response.delete_cookie('id')
            return response
        else:
            return render(request,'change_password.html',{'status':'Password incorrect','col':'danger'})
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    return render(request,'change_password.html',{'details':emp_det,'employee':emp,'notify':an,'totalnot':total})
def checkNotify():
    leaves = HrNotification.objects.filter(notify_type='leave',status=False)
    docs = HrNotification.objects.filter(notify_type='document',status=False)
    signup = HrNotification.objects.filter(notify_type='signup',status=False)
    notifys={
        'leaves':len(leaves),
        'signup':len(signup),
        'docs':len(docs),
    }
    return notifys

def hr_dash(request):
    user = HR.objects.get(user__username=request.user.username)
    nf = checkNotify()
    return render(request,'hr.html',{'notifications':nf})

def SignUp_Approve(request):
    user = User.objects.get(username=request.user.username)
    all = employee.objects.all()
    nf=checkNotify()
    if 'id' in request.GET:
        did = request.GET['id']
        action = request.GET['action']
        
        if action =="delete":
            job_obj = employee.objects.get(id=did)
            job_obj.delete()
        if action=="app":
            u_id = User.objects.get(id=did)
            emp=employee.objects.get(user__username=u_id.username)
            if u_id.is_active:
                u_id.is_active=False
                u_id.save()
            else:
                u_id.is_active=True
                u_id.save()
                return HttpResponseRedirect('/myApp/input_job?id='+str(emp.id))
        return HttpResponseRedirect('/myApp/SignUpApprove')
    return render(request,'SignupApproval.html',{'all':all,'notifications':nf})

def document_review(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)

    all_docs = document.objects.filter(doc=emp).order_by('-id')

    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)

    if request.method=="POST":
        d = request.FILES['document']
        r = request.POST['reference']
        ds=request.POST['description']

        docs=document(doc=emp,document=d,reference=r,description=ds)
        docs.save()
        return render(request,'documentReview.html',{'details':emp_det,'employee':emp,'status':'Document uploaded Successfullly!!','all_docs':all_docs})     

    return render(request,'documentReview.html',{'details':emp_det,'employee':emp,'all_docs':all_docs,'notify':an,'totalnot':total})

def leave_approval(request):
    all_leaves = leaveApply.objects.filter(status="Pending").order_by('-id')
    nf = checkNotify()
    if request.method == "POST":
        id = request.POST['leaveid']
        st = request.POST['status']
        print(request.POST)

        leave_obj = leaveApply.objects.get(id=id)
        leave_obj.status=st
        leave_obj.save()
        nf = checkNotify()
        empid=leave_obj.emp_id.user.id
        user_obj=User.objects.get(id=empid)
        
        msz = 'Leave Application {}!!!'.format(st)
        notify = Notification(user_id=user_obj,content=msz)
        notify.save()
        return render(request,'leaveApproval.html',{'all':all_leaves,'status':msz,'notifications':nf})

    return render(request,'leaveApproval.html',{'all':all_leaves,'notifications':nf})

def send_mail(request):
    user = User.objects.get(username = request.user.username)
    emp = employee.objects.get(user=user)

    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    if request.method=="POST":
        t = request.POST['to'].split(',')
        sb = request.POST['subject']
        msz = request.POST['message']

        msz_obj = EmailMessage(sb,msz,to=t)
        msz_obj.send()
        return render(request,'mail.html',{'status':'Email sent successfully!!','details':emp_det,'employee':emp})
    return render(request,'mail.html',{'data':user,'details':emp_det,'employee':emp,'notify':an,'totalnot':total})

def hrsend_mail(request):
    nf = checkNotify()
    if request.method=="POST":
        t = request.POST['to'].split(',')
        sb = request.POST['subject']
        msz = request.POST['message']   
        msz_obj = EmailMessage(sb,msz,to=t)
        msz_obj.send()
        return render(request,'hrmail.html',{'status':'Email sent successfully!!'})
    return render(request,'hrmail.html',{'notifications':nf})

def document_approval(request):
    user = HR.objects.get(user__username=request.user.username)
    all_docs = document.objects.all().order_by('-id')
    nf = checkNotify()
    if request.method == "POST":
        id = request.POST['docid']
        doc_obj = document.objects.get(id=id)
        st=""
        if "app" in request.POST: 
            st="Approve"
            doc_obj.status=st
            doc_obj.save()
        if "rej" in request.POST:
            st="Rejected!!"
            doc_obj.status=st
            doc_obj.delete()

        empid=doc_obj.doc.user.id
        user_obj=User.objects.get(id=empid)
        msz = 'Document {}!!!'.format(st)
        notify = Notification(user_id=user_obj,content=msz)
        notify.save()
        return HttpResponseRedirect('/myApp/documentAppprove')
    return render(request,'documentApproval.html',{'all':all_docs,'notifications':nf})

import json
def doc_Details(request):
    id = request.GET['id']
    dc = document.objects.get(id=id)
    detail ={
        'id':dc.id,
        'document':dc.document,
        'ref':str(dc.reference),
        'desc':str(dc.description),
        'sub_date':str(dc.submission_date),
    }
    dt = json.dumps(detail)
    return HttpResponse(dt,content_type="application/json") 

def hr_password(request):
    user = User.objects.get(username=request.user.username)
    login_user_password = request.user.password
    nf = checkNotify()
    if request.method == 'POST':
        current = request.POST['old']
        newpas = request.POST['new']

        check = check_password(current,login_user_password)
        if check==True:
            user.set_password(newpas)
            user.save()
            return render(request,'hr_change_password.html',{'status':'Password changed successfully','col':'success'})
            response = HttpResponseRedirect('/myApp/home')
            response.delete_cookie('id')
            return response
        else:
            return render(request,'hr_change_password.html',{'status':'Password incorrect','col':'danger'})
    # emp = employee.objects.get(user=user)
    # emp_det = emp_details.objects.get(emp_name=emp)
    # return render(request,'hr_change_password.html',{'details':emp_det,'employee':emp})
    return render(request,'hr_change_password.html',{'notifications':nf})

def announcements(request):
    user = HR.objects.get(user__username=request.user.username)
    nf = checkNotify()
    if request.method=="POST":
        txt = request.POST["text"]

        docs=announcement(hr_announce=user,text=txt)
        docs.save()
        return render(request,'announcementsUpdates.html',{'status':'Update successfully.....'})
    return render(request,'announcementsUpdates.html',{'notifications':nf})

def events(request):
    user = HR.objects.get(user__username=request.user.username)
    nf = checkNotify()
    if request.method=="POST":
        text = request.POST["event_text"]
        docu=event(hr_event=user,event_text=text)
        docu.save()
        return render(request,'eventUpdates.html',{'status':'Update successfully.....'})
    return render(request,'eventUpdates.html',{'notifications':nf})
  

import json
def leaveDetails(request):
    id = request.GET['id']
    lv = leaveApply.objects.get(id=id)
    detail ={
        'id':lv.id,
        'leave_type':lv.leave_type.ltype,
        'from_date':str(lv.from_date),
        'to_date':str(lv.to_date),
        'for_days':str(lv.for_days),
        'session':lv.session,
        'reason':lv.description,
        'sub_date':str(lv.sub_date),
    }
    dt = json.dumps(detail)
    return HttpResponse(dt,content_type="application/json")

def leave_assign(request):
    nf = checkNotify()
    if request.method == "POST":
        cl = request.POST['CL']
        pl = request.POST['PL']
        coff = request.POST['Coff']
        rh = request.POST['RH']
        cdate = request.POST['cdate']
        pdate = request.POST['pdate']
        coffdate = request.POST['coffdate']
        rdate = request.POST['rhdate']
        # return HttpResponse(date)
        
        getcls = leaveAssign.objects.filter(ltype="CL")
        if len(getcls)>=1:
            getcl = leaveAssign.objects.get(ltype="CL")
            getcl.total=cl
            getcl.date=cdate
            getcl.save()
        else:
            clss = leaveAssign(ltype="CL",total=cl,date=cdate)
            clss.save()

        getpls = leaveAssign.objects.filter(ltype="PL")
        if len(getpls)>=1:
            getpl = leaveAssign.objects.get(ltype="PL")
            getpl.total=pl
            getpl.date=pdate
            getpl.save()
        else:
            plss = leaveAssign(ltype="PL",total=pl,date=pdate)
            plss.save()

        getcoff = leaveAssign.objects.filter(ltype="Coff")
        if len(getcoff)>=1:
            getcoff = leaveAssign.objects.get(ltype="Coff")
            getcoff.total=coff
            getcoff.date=coffdate
            getcoff.save()
        else:
            coffss = leaveAssign(ltype="Coff",total=coff,date=coffdate)
            coffss.save()

        getrh = leaveAssign.objects.filter(ltype="RH")
        if len(getrh)>=1:
            getrh = leaveAssign.objects.get(ltype="RH")
            getrh.total=rh
            getrh.date=rdate
            getrh.save()
        else:
            rhss = leaveAssign(ltype="RH",total=rh,date=rdate)
            rhss.save()
      
        return render(request,'leaveAssign.html',{'status':'Assigned Successfully!!!'})
    return render(request,'leaveAssign.html',{'notifications':nf})

def forgot_password(request):
    if request.method=="GET":
        t = request.GET['username']

        check = User.objects.filter(username=t)
        if len(check)>=1:
            user =  User.objects.get(username=t)
            if user.is_staff:
                v = user.email
                otp = random.randrange(3000,4000)
                
                msz_obj = EmailMessage('Otp Verification',str(otp),to=[v,])
                msz_obj.send()
            if user.is_active:
                em = emp_details.objects.get(emp_name__user__username=user.username)
                to = em.email         
                otp = random.randrange(3000,4000)
                st = "Your OTP {}".format(otp)      
                msz_obj = EmailMessage('Otp Verification',st,to=[str(to),])
                msz_obj.send()
                return HttpResponse('An OTP Sent to Your Registred Email @'+str(otp))
            else:
                return HttpResponse('User with this name not found!!')
    
    return render(request,'newlogin.html')

def holiday_update(request):
    user = HR.objects.get(user__username=request.user.username)
    nf = checkNotify()
    if request.method=="POST":
        date = request.POST['date']
        day = request.POST['day']
        holi = request.POST['holiday']
        status = request.POST['status']

        docs=holiday(hr_holiday=user,date=date,day=day,holidays=holi,status=status)
        docs.save()
        return render(request,'holidayUpdates.html',{'status':'Updated successfully...'})  
    d = holiday.objects.all().order_by('date')
    response = HttpResponseRedirect('/myApp/holidayupdate')
        # return HttpResponseRedirect('/myApp/holidayupdate')
    return render(request,'holidayUpdates.html',{'d':d,'notifications':nf})

def addattendance(request):
    login_emp = employee.objects.get(user__username=request.user.username)

    if 'checkin' in request.POST:
        now = datetime.datetime.now()
        data = attendance(emp_name=login_emp,check_in_day=now.day,check_in_month=now.month,check_in_year=now.year)
        data.save()
        response.set_cookie('checkinid',data.id)
        return response

    if 'checkout' in request.POST:
        now = datetime.datetime.now()
        id = request.COOKIES['checkinid']
        data = attendance.objects.get(id=id)
        data.check_out_day=now.day
        data.check_out_month=now.month
        data.check_out_year=now.year
        data.save()

        response= HttpResponse('CheckOut Done')
        response.delete_cookie('checkinid')
        return response


def attendanceUser(request):
    
    login_emp = employee.objects.get(user__username=request.user.username)
    if ip in ip_list:
        if 'checkin' in request.POST:
            
            data = attendance_user(emp_name=login_emp,attendance_type='checkin')
            data.save()
            response=HttpResponseRedirect('/myApp/index/')
            response.set_cookie('checkintime',data.time)
            return response
        if 'checkout' in request.POST:
            
            data = attendance_user(emp_name=login_emp,attendance_type='checkout')
            data.save()
            response=HttpResponseRedirect('/myApp/index/')
            response.set_cookie('checkouttime',data.time)
            return response
        return response
    else:
        return HttpResponse('<h1>Sorry You are not allowed!!!</h1>')
from django.http import JsonResponse

def show_attendance(request):
    user = User.objects.get(username=request.user.username)
    emp = employee.objects.get(user=user)
    emp_det = emp_details.objects.get(emp_name=emp)
    an = Notification.objects.filter(user_id=user)
    unread = Notification.objects.filter(user_id=user,status=False)
    total = len(unread)
    return render(request,'showattendance.html',{'details':emp_det,'employee':emp,'notify':an,'totalnot':total})
def getattendance(request):
    login_emp = employee.objects.get(user__username=request.user.username)
    if 'month' in request.GET and 'year' in request.GET:
        month = request.GET['month']
        year = request.GET['year']
        data = list(attendance_user.objects.filter(emp_name=login_emp, date__month=month, date__year=year).values())
        
        
        return JsonResponse(data,safe=False) 

def changestatus(request):
    n = Notification.objects.filter(user_id__username=request.user.username)
    for i in n:
        s = Notification.objects.get(id=i.id)
        s.status=True
        s.save()
    return HttpResponseRedirect('/myApp/index')

def hr_changestatus(request):
    tp = request.GET['tp']
    n = HrNotification.objects.filter(notify_type=tp)
    # nf = checkNotify()
    for i in n:
        s = HrNotification.objects.get(id=i.id)
        s.status=True
        s.save()
    return HttpResponseRedirect('/myApp/hrdash')

def calendr(request):
    nf = checkNotify()
    return render(request,'calendar.html',{'notifications':nf})

def get_status(request):
    # uid = request.GET['uid']
    # v = employee.objects.get(user__id=uid)
    return render(request,'index.html')

def contact_us(request):
    if request.method=='POST':
        name = request.POST['name']
        email = request.POST['email']
        subject = request.POST['subject']
        message = request.POST['message']
        getData = contactus(name=name,email=email,subject=subject,message=message)
        getData.save()
        return render(request,'HomePage.html',{'status':'Update successfully.....'})
    return render(request,'HomePage.html',{'name':name})